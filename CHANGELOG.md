# Change Log

## 2.2.0

Update Job Override Rule to be able to also prevent jobs from running

### Added

- update job override rule to be able to also prevent jobs from running

## 2.1.3

Hotfix to add project-info.yaml file support to the build image pipeline

### Fixed

- include project-info.yaml file in build image pipeline

## 2.1.2

Hotfix for release job

### Fixed

- include project-info.yaml file directly in the release job so it pulls variables correctly

## 2.1.1

Hotfix for Only-Run-On-Main-Merge Ruleset.

### Changed

- add dont-run-on-mr-or-tag ruleset to release job as a just-in-case

### Fixed

- fix only-run-on-main-merge ruleset to run when expected

## 2.1.0

Add Job Override Rule.

### Added

- add new rule to every job that allows overriding all other rules and forcing the job to run if the OVERRIDE env var is set to the job's name
  - closes #9

## 2.0.2

Hotfix for Release Job.

### Fixed

- fix DATABASE_ENTRY JSON variable

## 2.0.1

Hotfix for Build Image Pipeline.

### Fixed

- fix matrix in build job of the build-image-pipeline

## 2.0.0

Project Overhaul and Refactor to follow better practices and consistent documentation.

### Added

- add LICENSE.txt file
- replace .gitlab-ci.yml with project-info.yaml file
- add resources.yaml file to hold GitLab CICD job components
  - all jobs now use this file

### Changed

- update README and CHANGELOG documentation to match the format set in project-boilerplate-generation
- update all pipeline and job internal documentation and their documentation in the README
- rename many files and now all use the .yaml extension
- update delete-tag job to be more abstract and not just delete the latest tag

## 1.4.0

Update Build Job to use Public Kaniko Image.

### Changed

- update image used in build job to the public kaniko image so kaniko can be removed from CICD toolkit

## 1.3.0

Add Master Branch Support.

### Added

- add support for 'master' branch in build, delete-latest-tag, and release jobs

## 1.2.0

Update Delete Latest Tag Job to Allow Failures.

### Changed

- update delete-latest-tag job to allow for job failure in the case that the latest tag does not exist

## 1.1.0

Build Job Maturation.

### Added

- add variable to build job called BUILD_ARGS to allow for build arguments when building an image
- add variable to build job called NO_PUSH to allow for image build testing and not having the image push anywhere
- add test-build stage to build-pipeline to alow for non-main branches to test the building of an image

### Changed

- update build job image to use cicd-toolkit:latest image

## 1.0.0

Stable 1.0.0 Release and Add Pre-Made Pipelines.

### Added

- add 'How to use' documentation for the pipeline and the jobs
- add pipelines directory to store various pipelines that other projects can use
  - add build-pipeline.yml for projects to create a new image
  - add release-pipeline.yml for every project to use so all project info is inserted into AWS

### Changed

- update repo-icon.png
- update variable documentation in each stage
- update 'stages' directory to the 'jobs' directory
- update release and delete-latest-tag jobs to no longer use the before_script to allow for easy extensibility

## 0.2.0

Add Delete Latest Tag job and Send Project Info to DynamoDB.

### Added

- add delete-latest-tag job
- add functionality to release job that sends project info to AWS DynamoDB
- add repo-icon.png

### Changed

- update rulesets and documentation for all jobs
- update jobs to use cicd-toolkit:latest image
- update README formatting to match custom format

### Fixed

- fix references of "dockerfile" to "Dockerfile"

## 0.1.1

Hotfix for AWS authentication.

### Changed

- fix authentication in build job
- fix build job for aws ecr building and added variables
- update config.json to use AWS credstore

## 0.1.0

Initial project setup.

### Added

- add initial files
- add release and build jobs
